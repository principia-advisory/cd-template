#!/bin/bash

if [ -d "$1" ];
then
    cd "$1"
    echo "Pulling Repository"
    git checkout main
    git pull origin main
    echo "Restarting docker-compose"
    docker-compose down
    docker image prune -af
    docker-compose build --force-rm --no-cache
    docker-compose up -d
else
    git clone https://$2:$3@gitlab.com/principia-advisory/$1.git
    cd "$1"
    echo "Starting up container"
    docker-compose up --build -d
fi