FROM python:3.9

WORKDIR /app

COPY requirements.txt ./requirements.txt

#setup the virtual environment
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt

EXPOSE 8501

COPY . /app


